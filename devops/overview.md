# SpringboardVR Devops Overview

Devops at Springboard aims to scalabale and reliable without much maintance by the developement team. The core technologies behind the devops are:

- Helm
- Gitlab CI/CD
- Kubernetes

Any infrastructure that isn't directly related to a project has it's confiuration in a git repo [here](https://gitlab.com/SpringboardVR/infrastructure/)

## Helm

It all starts with Helm chart that's located in every projects git repository. They will be found in the infrastruture repo under Helm Charts. For the most part the charts are very simple and just instruct Kubernetes how to deploy and manage the application. When an application gets pushed to a review app, staging or production what happens is our CI/CD pipeline will deploy out this helm chart and point kubernetes to run a docker image with the desired code. This means if you need to manually deploy out some code, you just need to use the `helm` cli and pump in the desired variables for your chart. Helm requires a pretty good understanding of Kubernetes before it will make sense, but you can read more about it here: [https://helm.sh/](https://helm.sh/)

## Gitlab CI/CD

We use the very popular and very simple Gitlab CI to automatically test and deploy our code in response to git events. The CI/CD setup is fairly simple and standardized across all of our apps. In general it executes the following steps:

- Build Docker Image
- Push Docker Image to Gitlab Registry for the Project
- Pull that same docker image, and run tests against it
- Deploy Helm chart, tell Kubernetes to use brand new Docker image

All of the configuration for the CI/CD can be found in each projects `.gitlab-ci.yaml` file. For the most part these files are the same across all apps, with a few small exceptions, like ENV vars, and docker tag names so be aware that if you change one, it's likely you'll want to change every other `.gitlab-ci.yaml` in the application. You can read more about gitlab ci here: [https://docs.gitlab.com/ee/ci/quick_start/README.html](https://docs.gitlab.com/ee/ci/quick_start/README.html)

## Kubernetes

Kubernetes actually scheudles and runs the applications. There's nothing special about our setup and to that end I'll leave you with a link to [Kubernetes Docs](https://kubernetes.io/)

## Gitlab CI Runner

We use a private Gitlab CI runner that is configured [here](https://gitlab.com/SpringboardVR/infrastructure/tree/master/gitlab-runner). It takes the form a Kubernetes deployment and config map.

##### Updating Config
Make your changes to the `config.yaml` file and run `kubectl apply -f config.yaml`. The gitlab runner will automatically pick up the changes.

##### Updating the Gitlab version
Make your changes to the `deployment.yaml` file and run `kubectl apply -f deployment.yaml`. This will automatically restart the Gitlab runner with the updated deployment.

## Kubernetes Ingress
So we expose all of our application through a small number of load balancers. We use the exremely popular [ingress-nginx](https://github.com/kubernetes/ingress-nginx) controller to make this happen. We deploy it using a the `stable/ingress-nginx` helm chart. All config files for ingresses can be found [here](https://gitlab.com/SpringboardVR/infrastructure/tree/master/ingress-nginx)

##### Updating an ingress configuration
Change some values in on the values files then run the command `helm upgrade -f {{REPLACE}}-values.yaml {{REPLACE}}-ingress stable/nginx-ingress` where `{{REPLACE}}` is the name of the values file you changed.

#### Create a new ingress
You can duplicate a values files, add or remove values based on the docs [here](https://github.com/kubernetes/charts/tree/master/stable/nginx-ingress) then run `helm install --name {{REPLACE}}-ingress -f {{REPLACE}}-values.yaml stable/nginx-ingress` where `{{REPLACE}}` is the name of the ingress you choose.

##### What about SSL, TLS, HTTPS?
All ingresses are configred to a use a wildcard certificate by default. These certificate is in the `ingress` kubernetes namespace and is managed by `cert-manager` You can find configuration for cert manager [here](https://gitlab.com/SpringboardVR/infrastructure/tree/master/ingress-nginx) and you can find docs for cert manager [here](https://github.com/jetstack/cert-manager)

## Monitoring and Logs

We use stackdriver for all of this! It's fairly simple and straight forward to use. You can find them all [here](https://app.google.stackdriver.com/account/login/?next=/kubernetes%3Fproject%3Dastute-charter-193718)

## Others

* We use Google Cloud for nearly all of our cloud provider needs.
* We store our docker files for apps inside of the [infrastructure git repo](ttps://gitlab.com/SpringboardVR/infrastructure)
* We use a prometheus deployment that's based off [this tutorial](https://cloud.google.com/monitoring/kubernetes-engine/prometheus) to collect and manage ingress stats.
* Metabase is configured in the [infrastructure git repo](ttps://gitlab.com/SpringboardVR/infrastructure)
* `k8s-custom-iptables` is part of how the nodes connect to managed redis in Google Cloud, more info [here](https://cloud.google.com/memorystore/docs/redis/connecting-redis-instance)
* The SSL cert for the documentation gitlab pages page will need to be swapped out with the new wildcard from the kubernetes cluster, bother Jamie when this happens because it's kind of an involved process. (sorry future reader)
