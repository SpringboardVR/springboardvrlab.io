# Devops Troubleshooting

## Helm Has No Deployed Releases!

This is because the first time an app gets deployed with helm, if it doesn't successfully start up and pass the liveness probe Helm will timeout. This causes a bug where Helm is stuck between this 'half-deployed-half-not-state'... It's stupid I know, but it's an easy fix.

Get the release name from the failed Helm error. Get someone with Helm control and run the command
```
helm del --purge [release name]
```

So if the error is `Error: UPGRADE FAILED: "content-api-staging" has no deployed released` then the command to fix it would be `helm del --purge content-api-staging`

## Manual Deploy

Sometimes the CI/CD doesn't work, or maybe just the deploy doesn't work. Never fear! Our devops set up is very simple and it's easy to manually deploy code with a few small steps.

First ask yourself this question:

### Did the CI/CD pass the build step? If no start here, if yes proceed to Step 3a:

#### Step 1: Get the image name right

First we need to package the code we need deployed into a Docker image and push it up to the registry. That requires we follow a very easy Docker image naming convetion

Run the following command replacing everything within the square brackets with their appropriate values.

> NOTE: Branch names with weird characters like `feature/whatever`
> Most of the time you will replace any weird character with a `-`, you can see examples of branch names [here](https://gitlab.com/SpringboardVR/api/container_registry)
> If your pushing code that's on master just use `master`

```bash
$imageName=registry.gitlab.com/springboardvr/[REPO NAME]/[BRANCH NAME]:[COMMIT REF]
```
> For example [this commit](https://gitlab.com/SpringboardVR/api/commit/672275d5dc8faf02a4113bc0fa0d1c0b7e645608) would look like this
> `registry.gitlab.com/springboardvr/api/master:672275d5dc8faf02a4113bc0fa0d1c0b7e645608`

Remember/copy that image name for future steps

#### Step 2: Login, Build, and Push the image

Log into Gitlab registry if you aren't already. When asked for credentials use your Gitlab username and password.
```bash
docker login registry.gitlab.com
docker build -t $imageName .
docker push $imageName
```

Proceed to Step 3B

#### Step 3a (only if build passed in the CI/CD):

Go to the deploy step of the failed pipeline. And find the line where it says what image it's going to use for the deploy.

![Image Line](./images/image-line.png)

#### Step 3b:

Log into [Google Cloud Console, Kubernetes Engine, Workloads](https://console.cloud.google.com/kubernetes/workload).

Find the deployment of the code your trying to push. Click it then at the top click 'edit'.

Find the line where the previous gitlab image was referenced:

![Kube Image Line](./images/kube-image-line.png>)

Replace that value with the image your trying to deploy, either the image you got from Step 1 or Step 3A.

Hit save.

> NOTE: Some applications are multiple deployments.
> For API there are multiple deployments that need to be updated, for example, `[ENVIRONMENT]-cron` and `[ENVIRONMENT]-worker`. Be sure to update the appropriate images when updating an application code.

#### Success!

Watch as the deployment rolling updates and everything in the world is back to normal.
