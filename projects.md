## Projects

SpringboardVR is a system built of many separate components! Here's an overview of each of them with some details.

### Backend PHP
#### [API](https://gitlab.com/SpringboardVR/api)
* Owner: Nicholas Bates, previously Matthew Hall
* Framework: Laravel
* URL: app.springboardvr.com / api.springboardvr.com

This is our main backbone for our whole platform! It handles users/organizations, billing, authentication, session tracking, etc. etc. We are slowly working on moving functionalities out of this application and bringing them into

#### [Public Website API]()
* Owner: Matthew Hall
* Framework: Laravel
* URL: public-api.springboardvr.com

This is a simple API for powering the public website at springboardvr.com. It works with the Public Website SSR frontend repo. It generates a GraphQL API for it to consume and has a Laravel Nova admin panel to allow members to login and edit posts and pages.

#### [Build API](https://gitlab.com/SpringboardVR/build)
* Owner: Matthew Hall
* Framework: Lumen
* URL: build-api.springboardvr.com

Currently a bit of a stub API just to make it so `springboardvr-cli upgrade` checks don't fail. Eventually will fully integrate with the

#### [Ad API](https://gitlab.com/SpringboardVR/ad-api)
* Owner: Matthew Hall
* Framework: Laravel
* URL: ad-api.springboardvr.com

Simple GraphQL API for the desktop client to be able to report ad impressions from Admix. Eventually could grow to a full Advertising API.

### Backend Go
#### [Content Distribution](https://gitlab.com/SpringboardVR/content-distribution)
* Owner: James Wetter, previously Matthew Hall
* Framework: None

Provides our backend for the springboardvr-cli tool. Handles storing game uploads and powering game downloads.

### Desktop Go
#### [springboardvr-cli](https://gitlab.com/SpringboardVR/springboardvr-cli)
* Owner: James Wetter, previously Matthew Hall
* Framework: None, fork of Butler

Fork of Butler by Itch.io. Provides a CLI tool for game developers to upload builds to us and a daemon for us to use in our desktop software to manage game downloads.

### Desktop C&#35;
Needs to be documented, not in Gitlab

### Frontend
#### [Operator Panel](https://gitlab.com/SpringboardVR/operator-panel)
* Owner: Jamie Spittal and Alexandre Geissman
* Framework: VueJS
* URL: operator.springboardvr.com

Management panel for our Operators.

#### [Developer Panel](https://gitlab.com/SpringboardVR/developer-panel)
* Owner: Jamie Spittal and Alexandre Geissman
* Framework: VueJS
* URL: developer.springboardvr.com

Management panel for our developers.

#### [Monitor](https://gitlab.com/SpringboardVR/monitor)
* Owner: Jamie Spittal
* Framework: VueJS
* URL: monitor.springboardvr.com

Where our operators live in their day to day. Shows all of their computers, whats playing on them, and lets them manage them remotely.

#### [Customer Panel](https://gitlab.com/SpringboardVR/customer-panel)
* Owner: Jamie Spittal
* Framework: VueJS
* URL: customer.springboardvr.com

Our Customer reservation and management application. Generates our booking system that operators can embed on their website.

#### [Station](https://gitlab.com/SpringboardVR/station)
* Owner: Alexandre Geissman
* Framework: VueJS
* URL: station.springboardvr.com

Our Station UI that is used inside of the VR headset by our desktop Launcher application.

#### [Public SSR](https://gitlab.com/SpringboardVR/public-ssr)
* Owner: Alexandre Geissman
* Framework: VueJS
* URL: springboardvr.com

Generates our new public site at springboardvr.com

### Other
#### [i18n](https://gitlab.com/SpringboardVR/i18n)

All of our translations! Managed in phraseapp.com which automatically syncs with our Github.com account, which we need to then manually sync with Gitlab because the Gitlab auto sync doesn't seem to work too well.

#### [go-springboardvr](https://gitlab.com/SpringboardVR/go-springboardvr)
* Owner: James Wetter, previously Matthew Hall

Fork of go-itchio by Itch.io. Provides bindings for our Go applications between the desktop client and the web server.

#### [Infrastructure](https://gitlab.com/SpringboardVR/infrastructure)
* Owner: Jamie Spittal

Things for Kubernetes and Devops-ey things!

#### [Gitlab CI Templates](https://gitlab.com/SpringboardVR/gitlab-ci-templates)
* Owner: Jamie Spittal

All of the templates used by Gitlab CI. Needs to be a public repo.

#### [Shared Frontend Resources](https://gitlab.com/SpringboardVR/shared-frontend-resources)
* Owner: Jamie Spittal and Alexandre Geissman

The name explains it! All used by the VueJS frontend applications.

#### [Frontend Shared Config](https://gitlab.com/SpringboardVR/frontend-shared-config)
* Owner: Jamie Spittal and Alexandre Geissman

Shared config files for VueJS CLI tool.
Confusing that this is named differently then Shared Fronted Resources...

#### [Sounds](https://gitlab.com/SpringboardVR/sounds)
* Owner: Jamie Spittal

All of the sound files for the music and actions inside our Launcher software.

