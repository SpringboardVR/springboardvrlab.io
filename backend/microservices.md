# Microservices

Our microservices infrastructure is currently in it's infancy. Right now we are just in the midst of getting RabbitMQ setup across all of our APIs using the [nuwber/rabbitevents](https://github.com/nuwber/rabbitevents/) package.


We are initially keeping our normal event dispatcher separate from RabbitMQ events. Eventually we will likely centralize all of this but for the time being you need to do a custom implmenetation of the [BroadcastEventServiceProvider](https://gitlab.com/SpringboardVR/ad-api/blob/master/app/Providers/BroadcastEventServiceProvider.php).

Currently interapp communication is just done with each server's specific GraphQL implementation. In the future we would like to centralize this in a more robust re-usable method of querying across services.
