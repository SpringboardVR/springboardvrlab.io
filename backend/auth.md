## Authentication

We have supported a myriad of different authentication methods in the past and you will see those littered throughout our existing products.

We offer an `authenticate` mutation in GraphQL that returns a `User` type.

```mutation {
  authenticateUser(email: "email@email.com", password: "somepassword") {
    id
    name
    email
    token
    organization {
      id
      name
      slug
    }
  }
}```

It returns a `token` field that is to be used for authenticated requests by using it as a header in the following format.
```Authorization: Bearer {token}```

The `token` field is only available on the `authenticateUser` mutation and isn't avaialble on any other queries such as `me {}`.

Going forward we are simplifying this with an eventual OAuth2 server implementation with token refreshing, but that hasn't been finalized yet.
