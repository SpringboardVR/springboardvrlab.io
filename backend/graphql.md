# GraphQL

Our GraphQL server runs at http://api.springboardvr.com/graphql

On any staging or development environment the GraphiQL interface is available at `http://staging-api.springboardvr.com/graphiql`

On these staging and development environments you can ghost into other organizations by specifying a tenant ID in the URL, such as

`http://staging-api.springboardvr.com/graphiql?tenant=524`


GraphQL will function without any authentication but the vast majority of queries and mutations won't work without authentication headers being set. Please see the [authentication documentation](auth.md) for more details.

Currently our GraphQL server is built using a fork of the [folklore/laravel-graphql](https://github.com/Folkloreatelier/laravel-graphql) package. Due to it's lack of maintenance, no support for writing schemas in Javascript, and general performance issues we are evaluating migrating to [nuwave/lighthouse](https://github.com/nuwave/lighthouse) in 2019.
